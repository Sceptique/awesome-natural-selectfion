#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <errno.h>

# define ERROR_PROB 570
# define ERROR_RANGE 2
# define NB_CHILD_MAX 12
# define NB_CHILD_MIN 6
# define REPRODUCT_TIME 50000

int	main(int ac, char **av)
{
  int	fdi;
  int	fdo;
  int	i;
  char	*child;
  char	c;

  (void)ac;
  srand(time(NULL));
  i = rand() % (NB_CHILD_MAX - NB_CHILD_MIN) + NB_CHILD_MIN + 1;
  while (i)
    {
      child = strndup(av[0], strlen(av[0]) + 1 + 1);
      child[strlen(av[0])] = '0' + i;
      child[strlen(av[0])+1] = '\0';
      write(1, child, strlen(child));
      write(1, "\n", 1);
      if ((fdi = open(av[0], O_RDONLY)) == -1)
	return 10;
      if ((fdo = open(child, O_WRONLY | O_CREAT,
		      S_IXUSR | S_IWUSR | S_IRUSR)) == -1)
	return 12;
      while ((read(fdi, &c, 1)) > 0)
	{
	  c = (rand() % ERROR_PROB ? c :
	       c + rand() % (ERROR_RANGE + 2) - ERROR_RANGE);
	  write(fdo, &c, 1);
	}
      close(fdi);
      close(fdo);
      system(child);
      free(child);
      i--;
      usleep(REPRODUCT_TIME);
    }
  char x[1000];
  (void)x;
  return (0);
}
